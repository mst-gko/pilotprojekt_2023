import time

import geopandas as gpd


def union_dataframe(gdf):
    # check if geometries are valid
    gdf = gdf[gdf.geometry.is_valid]

    # the union of all geometries
    union_geometry = gdf.union_all()
    union_gdf = gpd.GeoDataFrame(geometry=[union_geometry], crs=gdf.crs)

    return union_gdf


def intersect_iol(gdf, gdf_iol):
    gdf_overlap_iol = gpd.overlay(gdf, gdf_iol, how='intersection', keep_geom_type=True, make_valid=True)

    return gdf_overlap_iol


def calculate_intersection_area(gdf, gdf_overlap):
    # Dissolve the geometries of the two geo-dataframe
    gdf = union_dataframe(gdf)
    gdf_overlap = union_dataframe(gdf_overlap)

    # Calculate the area of the original geometry in hectares
    area_hectares = gdf.area.sum() / 10000

    # Get the intersection of the two GeoDataFrames and calculate the area in hectares
    intersection = gpd.overlay(gdf, gdf_overlap, how='intersection', keep_geom_type=True, make_valid=True)
    area_hectares_intersection = intersection.area.sum() / 10000

    # Calculate the percentage of the overlap geometry compared to original
    area_hectares_intersection_pct = 100 * area_hectares_intersection / area_hectares

    return area_hectares_intersection, area_hectares_intersection_pct


def overlap_analysis(name_gdf, gdf_faelles, gdf_for, gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol):
    # faelles
    gdf_faelles = intersect_iol(gdf_faelles, gdf_iol)
    fae_nfi, fae_nfi_pct = calculate_intersection_area(gdf_faelles, gdf_nfi)
    fae_skov, fae_skov_pct = calculate_intersection_area(gdf_faelles, gdf_skov)
    fae_bebyg, fae_bebyg_pct = calculate_intersection_area(gdf_faelles, gdf_bebyg)
    fae_mark, fae_mark_pct = calculate_intersection_area(gdf_faelles, gdf_marker2023)

    # forening
    gdf_for = intersect_iol(gdf_for, gdf_iol)
    for_nfi, for_nfi_pct = calculate_intersection_area(gdf_for, gdf_nfi)
    for_skov, for_skov_pct = calculate_intersection_area(gdf_for, gdf_skov)
    for_bebyg, for_bebyg_pct = calculate_intersection_area(gdf_for, gdf_bebyg)
    for_mark, for_mark_pct = calculate_intersection_area(gdf_for, gdf_marker2023)

    print(f'\n{name_gdf}')
    print('Fællesmængde')
    print(f'NFI: {fae_nfi.round(-2)} ({fae_nfi_pct.round(0)}%)')
    print(f'SKOV: {fae_skov.round(-2)} ({fae_skov_pct.round(0)}%)')
    print(f'BEBYGGELSE: {fae_bebyg.round(-2)} ({fae_bebyg_pct.round(0)}%)')
    print(f'MARKER: {fae_mark.round(-2)} ({fae_mark_pct.round(0)}%)')
    print('Foreningsmængde')
    print(f'NFI: {for_nfi.round(-2)} ({for_nfi_pct.round(0)}%)')
    print(f'SKOV: {for_skov.round(-2)} ({for_skov_pct.round(0)}%)')
    print(f'BEBYGGELSE: {for_bebyg.round(-2)} ({for_bebyg_pct.round(0)}%)')
    print(f'MARKER: {for_mark.round(-2)} ({for_mark_pct.round(0)}%)')


def run_overlap_analysis(path_nfi, path_skov, path_bebyg, path_marker2023, path_iol,
                         path_gvd_filter_faelles, path_gvd_filter_forening,
                         path_gvd_magasin_faelles, path_gvd_magasin_forening,
                         path_stok_filter_faelles, path_stok_filter_forening,
                         path_stok_magasin_faelles, path_stok_magasin_forening):

    gdf_nfi = gpd.read_file(path_nfi)
    gdf_skov = gpd.read_file(path_skov)
    gdf_bebyg = gpd.read_file(path_bebyg)
    gdf_marker2023 = gpd.read_file(path_marker2023)
    gdf_iol = gpd.read_file(path_iol)

    # gvd_filter
    gdf_gvd_filter_faelles = gpd.read_file(path_gvd_filter_faelles)
    gdf_gvd_filter_forening = gpd.read_file(path_gvd_filter_forening)
    overlap_analysis('GVD filter', gdf_gvd_filter_faelles, gdf_gvd_filter_forening,
                     gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol)

    # gvd_aquifer
    gdf_gvd_magasin_faelles = gpd.read_file(path_gvd_magasin_faelles)
    gdf_gvd_magasin_forening = gpd.read_file(path_gvd_magasin_forening)
    overlap_analysis('GVD magasin', gdf_gvd_magasin_faelles, gdf_gvd_magasin_forening,
                     gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol)

    # stochastic filter
    gdf_stok_filter_faelles = gpd.read_file(path_stok_filter_faelles)
    gdf_stok_filter_forening = gpd.read_file(path_stok_filter_forening)
    overlap_analysis('Stokastisk filter', gdf_stok_filter_faelles, gdf_stok_filter_forening,
                     gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol)

    # stochastic aquifer
    gdf_stok_magasin_faelles = gpd.read_file(path_stok_magasin_faelles)
    gdf_stok_magasin_forening = gpd.read_file(path_stok_magasin_forening)
    overlap_analysis('Stokastisk magasin', gdf_stok_magasin_faelles, gdf_stok_magasin_forening,
                     gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol)


start_time = time.time()  # start timer

root_path = r'F:\Nordjylland\Medarbejdere\MAKYN\Ongoing\QGIS_Forenings og fællesmængde'

# paths for common quantity
path_gvd_filter_faelles = fr'{root_path}\GRUNDVANDSDANNELSE_FILTER_Fællesmængde.gpkg'
path_gvd_magasin_faelles = fr'{root_path}\GRUNDVANDSDANNELSE_MAGASIN_Fællesmængde.gpkg'
path_stok_filter_faelles = fr'{root_path}\STOKASTISK_FILTER_Fællesmængde.gpkg'
path_stok_magasin_faelles = fr'{root_path}\STOKASTISK_MAGASIN_Fællesmængde.gpkg'

# paths for association quantity
path_gvd_filter_forening = fr'{root_path}\GRUNDVANDSDANNELSE_FILTER_Foreningsmængde.gpkg'
path_gvd_magasin_forening = fr'{root_path}\GRUNDVANDSDANNELSE_MAGASIN_Foreningsmængde.gpkg'
path_stok_filter_forening = fr'{root_path}\STOKASTISK_FILTER_Foreningsmængde.gpkg'
path_stok_magasin_forening = fr'{root_path}\STOKASTISK_MAGASIN_Foreningsmængde.gpkg'

# path for externally gis data for overlap analysis
path_nfi = fr'{root_path}\NFI.gpkg'
path_skov = fr'{root_path}\Skov_2023.gpkg'
path_bebyg = fr'{root_path}\Bebyggelse_Zonekort.gpkg'
path_marker2023 = fr'{root_path}\Marker.gpkg'
path_iol = fr'{root_path}\IOL.gpkg'

run_overlap_analysis(
    path_nfi, path_skov, path_bebyg, path_marker2023, path_iol,
    path_gvd_filter_faelles, path_gvd_filter_forening,
    path_gvd_magasin_faelles, path_gvd_magasin_forening,
    path_stok_filter_faelles, path_stok_filter_forening,
    path_stok_magasin_faelles, path_stok_magasin_forening
)

end_time = time.time()  # end timer
execution_time = end_time - start_time
print(f"\nExecution time: {execution_time:.2f} seconds")
