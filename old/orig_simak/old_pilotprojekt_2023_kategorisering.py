import time
import concurrent.futures

import pandas as pd
import geopandas as gpd
import numpy as np
# import openpyxl  # needed in py-env as engine for pandas' to_excel function


class AquiferClassification:

    def __init__(self, aquifer_name, path_gdo, path_gvd, path_stok, path_likely, path_iol, path_output='./'):
        """
        Initiates the class
        :param aquifer_name: [string] name of the specific aquifer
        :param path_gdo: [string] path to the geofile containing GDO
        :param path_gvd: [string] path to the geofile containing GVD
        :param path_stok: [string] path to the geofile containing stok
        :param path_likely: [string] path to the geofile containing likely
        :param path_output: [string] path of the exported files
        """
        self.aquifer_name = aquifer_name
        self.path_output = path_output
        self.gdf_gdo = self.get_geodataframe(path_gdo)
        self.gdf_gvd = self.get_geodataframe(path_gvd)
        self.gdf_stok = self.get_geodataframe(path_stok)
        self.gdf_likely = self.get_geodataframe(path_likely)
        self.gdf_iol = self.get_geodataframe(path_iol)
        self.gdf_merged = self.merge_dataframes()
        self.perform_data_wrangle()
        self.add_classification_static()
        self.add_classification_relative()
        self.gdf_merged_iol = self.crop_geodataframe_by_iol()

    @staticmethod
    def get_geodataframe(path_file):
        """
        This function input a geo file (e.g. shp, geopackage) and outputs a geopandas dataframe
        :param path_file: path for the geo file
        :return: geopandas dataframe
        """
        # import for geopandas dataframe
        gdf = gpd.read_file(path_file)

        return gdf

    @staticmethod
    def drop_geom(gdf):
        """
        This method inputs a geopandas dataframe and removes the geometry if it exists
        :param gdf: geopandas dataframe
        :return: pandas dataframe
        """
        # drop the geometry column if it exists
        if 'geometry' in gdf.columns:
            gdf = gdf.drop(columns=['geometry'])

        return gdf

    def merge_dataframes(self):
        """
        Merge grundvandsdannelse, grundvandsdannende opland, stokatiske kørsler and the likely wellfield
        The geometry of the gvd gdf is used rest of geometries are dropped
        :return: merged geopandas dataframe
        """

        df_z1 = pd.merge(
            self.gdf_gvd, self.drop_geom(self.gdf_gdo),
            left_on='Cell_ID', right_on='Cell_ID',
            how='outer', suffixes=('_gvd', '_gdo')
        )

        df_z2 = pd.merge(
            df_z1, self.drop_geom(self.gdf_stok),
            left_on='Cell_ID', right_on='CELLID',
            how='outer', suffixes=('', '_stok')
        )

        gdf_merged = pd.merge(
            df_z2, self.drop_geom(self.gdf_likely),
            left_on='Cell_ID', right_on='CELLID',
            how='outer', suffixes=('', '_likely')
        )

        return gdf_merged

    def perform_data_wrangle(self):
        """
        Performs data wrangle on the merged dataframe
        1) calculate gvd 3d for a year for the stochastic dataset
        2) calculate gvd 3d for a year for the likelyhood dataset
        """
        # calculate gvd 3d for a year for the stochastic dataset
        self.gdf_merged['GVD_3D_stok'] = self.gdf_merged["AVEGVD_MM_"] * -365.25

        # calculate gvd 3d for a year for the likelyhood dataset
        self.gdf_merged['GVD_3D_likely'] = self.gdf_merged["GVD2REG_MM"] * -365.25

    def export_merged_dataframe_as_excel(self):
        """
        Exports the merged geopandas dataframe as an MS Excel spreadsheet
        """
        file_path = f'{self.path_output}/{self.aquifer_name}_classification_merged.xlsx'
        self.gdf_merged.to_excel(file_path, engine="openpyxl")

        file_path = f'{self.path_output}/{self.aquifer_name}_classification_merged_iol.xlsx'
        self.gdf_merged_iol.to_excel(file_path, engine="openpyxl")

    def export_merged_dataframe_as_geopackage(self):
        """
        Exports the merged geopandas dataframe as a geopackage
        """
        output_path = f'{self.path_output}/{self.aquifer_name}_classification_merged.gpkg'
        self.gdf_merged.to_file(output_path, driver='GPKG')

        output_path_iol = f'{self.path_output}/{self.aquifer_name}_classification_merged_iol.gpkg'
        self.gdf_merged_iol.to_file(output_path_iol, driver='GPKG')

    def crop_geodataframe_by_iol(self, target_crs='EPSG:25832'):
        """
        Crops the merged dataframe with the iol dataset
        :return: geopandas dataframe cropped within the iol dataset
        """
        if self.gdf_merged.crs != target_crs:
            gdf1 = self.gdf_merged.to_crs(target_crs)
        else:
            gdf1 = self.gdf_merged

        if self.gdf_iol.crs != target_crs:
            gdf2 = self.gdf_iol.to_crs(target_crs)
        else:
            gdf2 = self.gdf_iol

        gdf1_joined = gdf1.sjoin(gdf2, how="inner")

        return gdf1_joined

    def get_merged_dataframe(self):
        """
        Exports the merged dataframe
        :return: Geopandas dataframe
        """
        return self.gdf_merged

    def get_merged_dataframe_iol(self):
        """
        Exports the merged dataframe within the iol dataset
        :return: Geopandas dataframe
        """
        return self.gdf_merged_iol

    @staticmethod
    def calculate_nonzero_quantile(df, column_name, q=10):
        """
        Calculate a given quantile from positive values of a column within a dataframe
        :param df: pandas dataframe
        :param column_name: name of column in the pandas dataframe
        :param q: the desired quantile e.g. 10 as the 10% quantile of the data
        :return: the number of the quantile of the data
        """
        non_zero_values = df[df[column_name] > 0]
        quantile = np.percentile(non_zero_values[column_name], q=q)

        return quantile

    def classification_relative(self, colname_output, colname_3dgvd, colname_trvl, colname_likely):
        """
        Performs the relative classification. NB the order for the classification is important. E.g. if class 1 is
        generated after class 3 then all within class 3 will be changed to class 1 since the requirements are lower.
        :param colname_output: the name of the dataset used for creating the output column name
        :param colname_3dgvd: the column name of the input-data 3d-gvd
        :param colname_trvl: the column name of the input-data traveltime
        :param colname_likely: the column name of the input-data likelyhood
        """
        likely_limit = 0.25  # the limit of the likelyhood
        trvl_limit = 200  # the limit of the traveltime
        q_trvl = (50, 30, 20)  # quantiles for traveltime e.g. 50 => 50% quantile
        q_gvd3d = (10, 25, 50)  # quantiles for 3d gvd e.g. 50 => 50% quantil
        cat_r = 0  # initial category
        name_cat = f'{colname_output}_classify_relative'

        self.gdf_merged[name_cat] = cat_r
        for q_t, q_g in zip(q_trvl, q_gvd3d):
            cat_r += 1
            name_t = f'{colname_output}_trvl_relative_q{q_t}'
            name_g = f'{colname_output}_gvd3d_relative_q{q_gvd3d[0]}'
            self.gdf_merged[name_t] = self.calculate_nonzero_quantile(
                df=self.gdf_merged, column_name=colname_trvl, q=q_t)
            self.gdf_merged[name_g] = self.calculate_nonzero_quantile(
                df=self.gdf_merged, column_name=colname_3dgvd, q=q_g)
            self.gdf_merged.loc[
                (self.gdf_merged[colname_likely] > likely_limit) &
                (self.gdf_merged[colname_trvl] < trvl_limit) &
                (self.gdf_merged[colname_3dgvd] >= self.gdf_merged[name_g]) &
                (self.gdf_merged[colname_trvl] <= self.gdf_merged[name_t]),
                name_cat] = cat_r

    def classification_static(self, colname_output, colname_3dgvd, colname_trvl):
        """
        Performs the static classification. NB the order for the classification is important. E.g. if class 1 is
        generated after class 3 then all within class 3 will be changed to class 1 since the requirements are lower.
        :param colname_output: the name of the dataset used for creating the output column name
        :param colname_3dgvd: the column name of the input-data 3d-gvd
        :param colname_trvl: the column name of the input-data traveltime
        """
        trvl_limit_lst = (30, 100, 200)  # limit for traveltime
        q_gvd3d = 10  # quantiles for 3d gvd e.g. 50 => 50% quantile
        cat_s = 0  # initial category
        name_g = f'{colname_output}_gvd3d_static_q{q_gvd3d}'
        self.gdf_merged[name_g] = self.calculate_nonzero_quantile(
            df=self.gdf_merged, column_name=colname_3dgvd, q=q_gvd3d)
        name_cat = f'{colname_output}_classify_static'

        # classify static
        self.gdf_merged[name_cat] = cat_s
        for trvl_limit in trvl_limit_lst:
            cat_s += 1
            self.gdf_merged.loc[
                (self.gdf_merged['TrvlYr_avg_gvd'] > 0) &
                (self.gdf_merged[colname_trvl] <= trvl_limit) &
                (self.gdf_merged[colname_3dgvd] >= self.gdf_merged[name_g]),
                name_cat] = cat_s

    def add_classification_static(self):
        """
        Runs the static classification for all dataset associated with the given aquifer
        """
        # GVD
        self.classification_static(colname_output='gvd', colname_3dgvd='GVD_3D_gvd', colname_trvl='TrvlYr_avg_gvd')

        # GDO
        self.classification_static(colname_output='gdo', colname_3dgvd='GVD_3D_gdo', colname_trvl='TrvlYr_avg_gdo')

        # STOCHASTIC
        self.classification_static(colname_output='stok', colname_3dgvd='GVD_3D_stok', colname_trvl='AVEAGE[Y]')

        # LIKELYWELLFIELD
        self.classification_static(colname_output='likely', colname_3dgvd='GVD_3D_likely', colname_trvl='AVERAGEAGE')

    def add_classification_relative(self):
        """
        Runs the relative classification for all dataset associated with the given aquifer
        """
        # GVD
        self.classification_relative(
            colname_output='gvd', colname_3dgvd='GVD_3D_gvd',
            colname_trvl='TrvlYr_avg_gvd', colname_likely='PctPT_gvd'
        )

        # GDO
        self.classification_relative(
            colname_output='gdo', colname_3dgvd='GVD_3D_gdo',
            colname_trvl='TrvlYr_avg_gdo', colname_likely='PctPT_gdo'
        )

        # STOCHASTIC
        self.classification_relative(
            colname_output='stok', colname_3dgvd='GVD_3D_stok',
            colname_trvl='AVEAGE[Y]', colname_likely='LIKELY'
        )

        # LIKELYWELLFIELD
        self.classification_relative(
            colname_output='likely', colname_3dgvd='GVD_3D_likely',
            colname_trvl='AVERAGEAGE', colname_likely='LIKELYHOOD'
        )


def run_script(input_data_lst):
    """
    Function solely used when running the script concurrently
    Export a geopackage but not an Excel file per default
    :param input_data_lst: list containing all input data for an aquifer
    """
    AquiferClassification(
        aquifer_name=input_data_lst[0], path_gdo=input_data_lst[1], path_gvd=input_data_lst[2],
        path_stok=input_data_lst[3], path_likely=input_data_lst[4], path_iol=input_data_lst[5],
        path_output=input_data_lst[6]
    ).export_merged_dataframe_as_geopackage()


def run_script_concurrently(inputs_lst):
    """
    Function for running the script concurrently
    :param inputs_lst: list of lists containing all input data for each aquifer
    """
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit the tasks for each input
        futures = [executor.submit(run_script, data_lst) for data_lst in inputs_lst]

        # Wait for all tasks to complete
        concurrent.futures.wait(futures)


start_time = time.time()  # start timer

root_path_output = './output_data/'

# Aquifer 1
aquifer_name_sand1 = 'Sand1'
path_gdo_sand1 = r'C:\gitlab\pilotprojekt_2023\input_data\01_GDO_boringer\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand1.shp'
path_gvd_sand1 = r'C:\gitlab\pilotprojekt_2023\input_data\03_3D_GVD\Fyn_3D_GVD_RegCode1_PT_lag1B_mult_zlag1.shp'
path_stok_sand1 = r'C:\gitlab\pilotprojekt_2023\input_data\Stat_RegCode_Fyn\Stats_RegCode_1_sel.shp'
path_likely_sand1 = r'C:\gitlab\pilotprojekt_2023\input_data\Likely_Wellfield\Likely_Wellfield_beregninger.shp'
path_iol_sand1 = r'C:\gitlab\pilotprojekt_2023\input_data\iol_fyn\Sand1_IOL_BUFFER.gpkg'
data_aquifer1 = [
    aquifer_name_sand1, path_gdo_sand1, path_gvd_sand1, path_stok_sand1,
    path_likely_sand1, path_iol_sand1, root_path_output
]

# Aquifer 2
aquifer_name_sand2 = 'Sand2'
path_gdo_sand2 = r'C:\gitlab\pilotprojekt_2023\input_data\01_GDO_boringer\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand2.shp'
path_gvd_sand2 = r'C:\gitlab\pilotprojekt_2023\input_data\03_3D_GVD\Fyn_3D_GVD_RegCode2_PT_lag1B_mult_zlag1.shp'
path_stok_sand2 = r'C:\gitlab\pilotprojekt_2023\input_data\Stat_RegCode_Fyn\Stat_RegCode_2_sel.shp'
path_likely_sand2 = r'C:\gitlab\pilotprojekt_2023\input_data\Likely_Wellfield\Likely_Wellfield_beregninger.shp'
path_iol_sand2 = r'C:\gitlab\pilotprojekt_2023\input_data\iol_fyn\Sand2_IOL_BUFFER.gpkg'
data_aquifer2 = [
    aquifer_name_sand2, path_gdo_sand2, path_gvd_sand2, path_stok_sand2,
    path_likely_sand2, path_iol_sand2, root_path_output
]

# Aquifer 3
aquifer_name_sand3 = 'Sand3'
path_gdo_sand3 = r'C:\gitlab\pilotprojekt_2023\input_data\01_GDO_boringer\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand3.shp'
path_gvd_sand3 = r'C:\gitlab\pilotprojekt_2023\input_data\03_3D_GVD\Fyn_3D_GVD_RegCode3_PT_lag1B_mult_zlag1.shp'
path_stok_sand3 = r'C:\gitlab\pilotprojekt_2023\input_data\Stat_RegCode_Fyn\Stat_RegCode_3_sel.shp'
path_likely_sand3 = r'C:\gitlab\pilotprojekt_2023\input_data\Likely_Wellfield\Likely_Wellfield_beregninger.shp'
path_iol_sand3 = r'C:\gitlab\pilotprojekt_2023\input_data\iol_fyn\Sand3_IOL_BUFFER.gpkg'
data_aquifer3 = [
    aquifer_name_sand3, path_gdo_sand3, path_gvd_sand3, path_stok_sand3,
    path_likely_sand3, path_iol_sand3, root_path_output
]

# Aquifer 4
aquifer_name_kalk = 'Kalk'
path_gdo_kalk = r'C:\gitlab\pilotprojekt_2023\input_data\01_GDO_boringer\Fyn_GDO_PT_Lag1B_Wells_Stat_DK.shp'
path_gvd_kalk = r'C:\gitlab\pilotprojekt_2023\input_data\03_3D_GVD\Fyn_3D_GVD_RegCode4_PT_lag1B_mult_zlag1.shp'
path_stok_kalk = r'C:\gitlab\pilotprojekt_2023\input_data\Stat_RegCode_Fyn\Stat_RegCode_4_sel.shp'
path_likely_kalk = r'C:\gitlab\pilotprojekt_2023\input_data\Likely_Wellfield\Likely_Wellfield_beregninger.shp'
path_iol_kalk = r'C:\gitlab\pilotprojekt_2023\input_data\iol_fyn\Kalk_IOL_BUFFER.gpkg'
data_aquifer4 = [
    aquifer_name_kalk, path_gdo_kalk, path_gvd_kalk, path_stok_kalk,
    path_likely_kalk, path_iol_kalk, root_path_output
]

# pack data for all aquifers in a list of lists and run script for all aquifers concurrently
data_aquifer_all = [data_aquifer1, data_aquifer2, data_aquifer3, data_aquifer4]
run_script_concurrently(inputs_lst=data_aquifer_all)

end_time = time.time()  # end timer
execution_time = end_time - start_time
print(f"Execution time: {execution_time:.2f} seconds")
