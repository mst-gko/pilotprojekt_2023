import pandas as pd

project = QgsProject().instance()

sti = 'F:/Nordjylland/Medarbejdere/MAKYN/Ongoing/Indvindingsoplande_Klassificering/Resultater'

for x in ["Sand1", "Sand2", "Sand3", "Kalk"]: #
        
    data_file = x
    
    directory = f"{sti}/{data_file}/"
    # Check if the directory already exists
    if not os.path.exists(directory):
            # Create the directory
            os.makedirs(directory)
            print("Directory created successfully!")
    else:
            print("Directory already exists!")  



    IOL_Sand = project.mapLayersByName(f'{data_file}_IOL_Buffer')[0]
    
    Sand1_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand1')[0]
    Sand2_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand2')[0]
    Sand3_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand3')[0]
    Kalk_GDO  = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_DK')[0]
    Sand1_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode1_PT_lag1B_mult_zlag1')[0]
    Sand2_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode2_PT_lag1B_mult_zlag1')[0]
    Sand3_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode3_PT_lag1B_mult_zlag1')[0]
    Kalk_GVD  = project.mapLayersByName('Fyn_3D_GVD_RegCode4_PT_lag1B_mult_zlag1')[0]
    Sand1_STOK = project.mapLayersByName('Stats_RegCode_1_sel')[0]
    Sand2_STOK = project.mapLayersByName('Stat_RegCode_2_sel')[0]
    Sand3_STOK = project.mapLayersByName('Stat_RegCode_3_sel')[0]
    Kalk_STOK  = project.mapLayersByName('Stat_RegCode_4_sel')[0]
    LIKELYWELLFIELD_STOK  = project.mapLayersByName('Likely_Wellfield_beregninger')[0]


    if data_file == 'Sand1':
            layers = [Sand1_GVD, Sand1_GDO, Sand1_STOK, LIKELYWELLFIELD_STOK]
    elif data_file == 'Sand2':
            layers = [Sand2_GVD, Sand2_GDO, Sand2_STOK, LIKELYWELLFIELD_STOK]
    elif data_file == 'Sand3':
            layers = [Sand3_GVD, Sand3_GDO, Sand3_STOK, LIKELYWELLFIELD_STOK]
    elif data_file == 'Kalk':
            layers = [Kalk_GVD, Kalk_GDO, Kalk_STOK, LIKELYWELLFIELD_STOK]


    for feature in IOL_Sand.getFeatures():
            
            df =  pd.DataFrame(columns=['Anlægsnavn', 'AnlægsID', 'Indvindingstilladelse', 'GVD_Cell_ID', 'GVD_GVD_3D', 'GVD_PctPT', 'GVD_TrvlYr_avg'])

            df2 = pd.DataFrame(columns=['Anlægsnavn', 'AnlægsID', 'Indvindingstilladelse', 'GDO_Cell_ID', 'GDO_GVD_3D',  'GDO_PctPT', 'GDO_TrvlYr_avg'])

            df3 =  pd.DataFrame(columns=['Anlægsnavn', 'AnlægsID', 'Indvindingstilladelse','STOKASTISK_CELLID', 'STOKASTISK_AVEAGE[Y]', 'STOKASTISK_STDEVAGE[Y','STOKASTISK_LIKELY', 'STOKASTISK_AVEGVD', 'STOKASTISK_STDGVD'])

            df4 =  pd.DataFrame(columns=['Anlægsnavn', 'AnlægsID', 'Indvindingstilladelse', 'LIKELYWELLFIELD_CELLID', 'LIKELYWELLFIELD_GVD','LIKELYWELLFIELD_STDGVD', 'LIKELYWELLFIELD_LIKLY', 'LIKELYWELLFIELD_AVERAGEAGE', 'LIKELYWELLFIELD_STDAGE'])


            IOL_Sand.removeSelection()
            IOL_Sand.select(feature.id())
            
            layers[0].removeSelection()
            layers[1].removeSelection()
            layers[2].removeSelection()
            layers[3].removeSelection()
            
            processing.run("native:selectbylocation", {'INPUT': layers[0],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
            processing.run("native:selectbylocation", {'INPUT': layers[1],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
            processing.run("native:selectbylocation", {'INPUT': layers[2],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
            processing.run("native:selectbylocation", {'INPUT': layers[3],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
            
            fid = feature['fid']
            a = feature['anl_navn']
            b = feature['anl_id']
            c = feature['indvtil']
            
            
            for feat in layers[0].selectedFeatures():
                 df = df.append({'Anlægsnavn': a, 'AnlægsID': b, 'Indvindingstilladelse': c, 'GVD_Cell_ID': feat["Cell_ID"], 'GVD_GVD': feat["GVD"], 'GVD_GVD_3D': feat["GVD_3D"], 'GVD_PctPT': feat["PctPT"], 'GVD_TrvlYr_avg': feat["TrvlYr_avg"]}, ignore_index=True)


            for feat in layers[1].selectedFeatures():
                df2 = df2.append({'Anlægsnavn': a, 'AnlægsID': b, 'Indvindingstilladelse': c, 'GDO_Cell_ID': feat["Cell_ID"], 'GDO_GVD_3D': feat["GVD_3D"],  'GDO_PctPT': feat["PctPT"], 'GDO_TrvlYr_avg': feat["TrvlYr_avg"]}, ignore_index=True)

            for feat in layers[2].selectedFeatures():
                df3 = df3.append({'Anlægsnavn': a, 'AnlægsID': b, 'Indvindingstilladelse': c, 'STOKASTISK_CELLID': feat["CELLID"], 'STOKASTISK_AVEAGE[Y]': feat["AVEAGE[Y]"], 'STOKASTISK_STDEVAGE[Y': feat["STDEVAGE[Y"], 'STOKASTISK_LIKELY': feat["LIKELY"], 'STOKASTISK_AVEGVD': feat["AVEGVD_MM_"], 'STOKASTISK_STDGVD': feat["STDEVGVD_M"]}, ignore_index=True)

            for feat in layers[3].selectedFeatures():
                df4 = df4.append({'Anlægsnavn': a, 'AnlægsID': b, 'Indvindingstilladelse': c, 'LIKELYWELLFIELD_CELLID': feat["CELLID"], 'LIKELYWELLFIELD_GVD': feat["GVD2REG_MM"], 'LIKELYWELLFIELD_STDGVD': feat["STDVGVD"], 'LIKELYWELLFIELD_LIKLY': feat["LIKELYHOOD"], 'LIKELYWELLFIELD_AVERAGEAGE': feat["AVERAGEAGE"], 'LIKELYWELLFIELD_STDAGE': feat["STDVAGE"]}, ignore_index=True)
            
            try:
                df_z1 = pd.merge(df, df2, left_on='GVD_Cell_ID', right_on='GDO_Cell_ID')
                df_z2 = pd.merge(df_z1, df3, left_on='GVD_Cell_ID', right_on='STOKASTISK_CELLID')
                df_z3 = pd.merge(df_z2, df4, left_on='GVD_Cell_ID', right_on='LIKELYWELLFIELD_CELLID')

                layers[0].removeSelection()
                layers[1].removeSelection()
                layers[2].removeSelection()
                layers[3].removeSelection()
                    
                IOL_Sand.removeSelection()
                print(df_z3)
                
                df_z3.to_excel(f"{sti}/{data_file}/{a.replace('/','-').replace(':', '-')} - {fid}.xlsx", engine="openpyxl")
            except:
                pass