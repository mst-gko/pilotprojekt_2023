import glob
import pandas as pd
import re
import numpy as np
import os
import sys

project = QgsProject().instance()

for x in ["Sand1", "Sand2", "Sand3", "Kalk"]:
    
        data_file = x

        IOL_Sand = project.mapLayersByName(f'{data_file}_IOL_Buffer')[0]

        Sand1_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand1')[0]
        Sand2_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand2')[0]
        Sand3_GDO = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_Sand3')[0]
        Kalk_GDO  = project.mapLayersByName('Fyn_GDO_PT_Lag1B_Wells_Stat_DK')[0]
        Sand1_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode1_PT_lag1B_mult_zlag1')[0]
        Sand2_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode2_PT_lag1B_mult_zlag1')[0]
        Sand3_GVD = project.mapLayersByName('Fyn_3D_GVD_RegCode3_PT_lag1B_mult_zlag1')[0]
        Kalk_GVD  = project.mapLayersByName('Fyn_3D_GVD_RegCode4_PT_lag1B_mult_zlag1')[0]
        Sand1_STOK = project.mapLayersByName('Stats_RegCode_1_sel')[0]
        Sand2_STOK = project.mapLayersByName('Stat_RegCode_2_sel')[0]
        Sand3_STOK = project.mapLayersByName('Stat_RegCode_3_sel')[0]
        Kalk_STOK  = project.mapLayersByName('Stat_RegCode_4_sel')[0]
        LIKELYWELLFIELD_STOK  = project.mapLayersByName('Likely_Wellfield_beregninger')[0]


        if data_file == 'Sand1':
            layers = [Sand1_GVD, Sand1_GDO, Sand1_STOK, LIKELYWELLFIELD_STOK]
            meanvalues = [54.3, 6.2, 36.5, 12] #Q10 regnet for hele Fyn
        elif data_file == 'Sand2':
            layers = [Sand2_GVD, Sand2_GDO, Sand2_STOK, LIKELYWELLFIELD_STOK]
            meanvalues = [59.1, 5.3, 36.5, 11.9] #Q10 regnet for hele Fyn
        elif data_file == 'Sand3':
            layers = [Sand3_GVD, Sand3_GDO, Sand3_STOK, LIKELYWELLFIELD_STOK]
            meanvalues = [23.3, 7.7, 36.5, 12.4] #Q10 regnet for hele Fyn
        elif data_file == 'Kalk':
            layers = [Kalk_GVD, Kalk_GDO, Kalk_STOK, LIKELYWELLFIELD_STOK]
            meanvalues = [16.8, 6.8, 36.5, 12.3] #Q10 regnet for hele Fyn

           
        
           
           
        og_path = f"F:/Nordjylland/Medarbejdere/MAKYN/Ongoing/Indvindingsoplande_Klassificering/Resultater/{data_file}/"
        path = f"{og_path}*.xlsx"

        for fname in glob.glob(path):
            try:
                IOL_Sand.removeSelection()

                temp = fname.split("/")[-1]
                navn_fid = temp.split('\\')[-1]
                navn_fid = navn_fid[:-5]
                fid = re.findall(r'\d+', fname)[-1]
                anlaeg = navn_fid[:-4] + f" {data_file}"
                print(anlaeg)
                
                sti = os.path.join(og_path, anlaeg)
                
                #-------------------------#  Udregner statistik  #-------------------------#
                df2 = pd.read_excel("{}".format(fname))
                df2["STOK_mmPrYearAVG"] = df2["STOKASTISK_AVEGVD"]*-365.25
                df2["STOK_stdGVDYear"] = df2["STOKASTISK_STDGVD"]*365.25
                df2["LIKELYWELLFIELD_mmPrYearAVG"] = df2["LIKELYWELLFIELD_GVD"]*-365.25
                df2["LIKELYWELLFIELD_stdGVDYear"] = df2["LIKELYWELLFIELD_STDGVD"]*365.25
                
                GVD_df = df2[(df2['GVD_GVD_3D'] > 0)]
                GDO_df = df2[(df2['GDO_GVD_3D'] > 0)]
                STOK_df = df2[(df2['STOK_mmPrYearAVG'] > 0)]
                LIKELYWELLFIELD_df = df2[(df2['LIKELYWELLFIELD_mmPrYearAVG'] > 0)]

                GVD_TT_Q50 = np.percentile(GVD_df['GVD_TrvlYr_avg'],50)
                GVD_TT_Q30 = np.percentile(GVD_df['GVD_TrvlYr_avg'],30)
                GVD_TT_Q20 = np.percentile(GVD_df['GVD_TrvlYr_avg'],20)
                
                GVD_GVD_Q10 = np.percentile(GVD_df['GVD_GVD_3D'],10)
                GVD_GVD_Q25 = np.percentile(GVD_df['GVD_GVD_3D'], 25)
                GVD_GVD_Q50 = np.percentile(GVD_df['GVD_GVD_3D'],50)


                GDO_TT_Q50 = np.percentile(GDO_df['GDO_TrvlYr_avg'],50)
                GDO_TT_Q30 = np.percentile(GDO_df['GDO_TrvlYr_avg'],30)
                GDO_TT_Q20 = np.percentile(GDO_df['GDO_TrvlYr_avg'],20)
                
                GDO_GVD_Q10 = np.percentile(GDO_df['GDO_GVD_3D'],10)
                GDO_GVD_Q25 = np.percentile(GDO_df['GDO_GVD_3D'], 25)
                GDO_GVD_Q50 = np.percentile(GDO_df['GDO_GVD_3D'],50)


                STOK_TT_Q50 = np.percentile(STOK_df['STOKASTISK_AVEAGE[Y]'],50)
                STOK_TT_Q30 = np.percentile(STOK_df['STOKASTISK_AVEAGE[Y]'],30)
                STOK_TT_Q20 = np.percentile(STOK_df['STOKASTISK_AVEAGE[Y]'],20)
                
                STOK_GVD_Q10 = np.percentile(STOK_df['STOK_mmPrYearAVG'],10)
                STOK_GVD_Q25 = np.percentile(STOK_df['STOK_mmPrYearAVG'], 25)
                STOK_GVD_Q50 = np.percentile(STOK_df['STOK_mmPrYearAVG'],50)

                LIKELYWELLFIELD_TT_Q50 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_AVERAGEAGE'],50)
                LIKELYWELLFIELD_TT_Q30 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_AVERAGEAGE'],30)
                LIKELYWELLFIELD_TT_Q20 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_AVERAGEAGE'],20)

                LIKELYWELLFIELD_GVD_Q10 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_mmPrYearAVG'],10)
                LIKELYWELLFIELD_GVD_Q25 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_mmPrYearAVG'], 25)
                LIKELYWELLFIELD_GVD_Q50 = np.percentile(LIKELYWELLFIELD_df['LIKELYWELLFIELD_mmPrYearAVG'],50)
                
                
                #-------------------------#  Relative kriterier  #-------------------------#
                IOL_Sand.selectByExpression(f"\"fid\"={fid}")

                for l in layers:
                    l.removeSelection()


                processing.run("native:selectbylocation", {'INPUT': layers[0],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[1],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[2],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[3],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                    

                processing.run("qgis:selectbyexpression", {'INPUT': layers[0],'EXPRESSION':'"TrvlYr_avg" < 200 and "TrvlYr_avg" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[1],'EXPRESSION':'"TrvlYr_avg" < 200 and "TrvlYr_avg" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[2],'EXPRESSION':'"AVEAGE[Y]" < 200 and "AVEAGE[Y]" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[3],'EXPRESSION':'"AVERAGEAGE" < 200 and "AVERAGEAGE" > 0','METHOD':3})
                #GVD
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[0].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_relativ','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD_3D" >= {GVD_GVD_Q50} AND "TrvlYr_avg" <= {GVD_TT_Q20} AND "PctPt" > 0.25 AND "TrvlYr_avg" < 200 THEN 3\r\nWHEN "GVD_3D" >= {GVD_GVD_Q25} AND "TrvlYr_avg" <= {GVD_TT_Q30} AND "PctPt" > 0.25  AND "TrvlYr_avg" < 200 THEN 2\r\nWHEN "GVD_3D" >= {GVD_GVD_Q10} AND "TrvlYr_avg" <= {GVD_TT_Q50} AND "PctPt" > 0.25  AND "TrvlYr_avg" < 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}GVD_{navn_fid}_relativekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[1].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_relativ','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD_3D" >= {GDO_GVD_Q50} AND "TrvlYr_avg" <= {GDO_TT_Q20} AND "PctPt" > 0.25 AND "TrvlYr_avg" < 200 THEN 3\r\nWHEN "GVD_3D" >= {GDO_GVD_Q25} AND "TrvlYr_avg" <= {GDO_TT_Q30} AND "PctPt" > 0.25  AND "TrvlYr_avg" < 200 THEN 2\r\nWHEN "GVD_3D" >= {GDO_GVD_Q10} AND "TrvlYr_avg" <= {GDO_TT_Q50} AND "PctPt" > 0.25  AND "TrvlYr_avg" < 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}GDO_{navn_fid}_relativekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[2].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_relativ','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "AVEGVD_MM_"*-365.25 >= {STOK_GVD_Q50} AND "AVEAGE[Y]" <= {STOK_TT_Q20} AND "LIKELY" > 0.25 AND "AVEAGE[Y]" < 200 THEN 3\r\nWHEN "AVEGVD_MM_"*-365.25 >= {STOK_GVD_Q25} AND "AVEAGE[Y]" <= {STOK_TT_Q30} AND "LIKELY" > 0.25  AND "AVEAGE[Y]" < 200 THEN 2\r\nWHEN "AVEGVD_MM_"*-365.25 >= {STOK_GVD_Q10} AND "AVEAGE[Y]" <= {STOK_TT_Q50} AND "LIKELY" > 0.25  AND "AVEAGE[Y]" < 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}STOK_{navn_fid}_relativekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[3].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_relativ','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD2REG_MM"*-365.25 >= {LIKELYWELLFIELD_GVD_Q50} AND "AVERAGEAGE" <= {LIKELYWELLFIELD_TT_Q20} AND "LIKELYHOOD" > 0.25 AND "AVERAGEAGE" < 200 THEN 3\r\nWHEN "GVD2REG_MM"*-365.25 >= {LIKELYWELLFIELD_GVD_Q25} AND "AVERAGEAGE" <= {LIKELYWELLFIELD_TT_Q30} AND "LIKELYHOOD" > 0.25  AND "AVERAGEAGE" < 200 THEN 2\r\nWHEN "GVD2REG_MM"*-365.25 >= {LIKELYWELLFIELD_GVD_Q10} AND "AVERAGEAGE" <= {LIKELYWELLFIELD_TT_Q50} AND "LIKELYHOOD" > 0.25  AND "AVERAGEAGE" < 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}LIKELYWELLFIELD_{navn_fid}_relativekriterier.gpkg'})




                #-------------------------#  Faste kriterier  #-------------------------#
                IOL_Sand.selectByExpression(f"\"fid\"={fid}")

                for l in layers:
                    l.removeSelection()
                    
                processing.run("native:selectbylocation", {'INPUT': layers[0],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[1],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[2],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                processing.run("native:selectbylocation", {'INPUT': layers[3],'PREDICATE':[0,5,6],'INTERSECT':QgsProcessingFeatureSourceDefinition(IOL_Sand.source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'METHOD':0})
                
                
                processing.run("qgis:selectbyexpression", {'INPUT': layers[0],'EXPRESSION':'"TrvlYr_avg" < 200 and "TrvlYr_avg" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[1],'EXPRESSION':'"TrvlYr_avg" < 200 and "TrvlYr_avg" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[2],'EXPRESSION':'"AVEAGE[Y]" < 200 and "AVEAGE[Y]" > 0','METHOD':3})
                processing.run("qgis:selectbyexpression", {'INPUT': layers[3],'EXPRESSION':'"AVERAGEAGE" < 200 and "AVERAGEAGE" > 0','METHOD':3})


                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[0].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_faste','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD_3D" >= {meanvalues[0]} AND "TrvlYr_avg" <= 30 THEN 3\r\nWHEN "GVD_3D" >= {meanvalues[0]} AND "TrvlYr_avg" <= 100 THEN 2\r\nWHEN "GVD_3D" >= {meanvalues[0]} AND "TrvlYr_avg" <= 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}GVD_{navn_fid}_fastekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[1].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_faste','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD_3D" >= {meanvalues[1]} AND "TrvlYr_avg" <= 30 THEN 3\r\nWHEN "GVD_3D" >= {meanvalues[1]} AND "TrvlYr_avg" <= 100  THEN 2\r\nWHEN "GVD_3D" >= {meanvalues[1]} AND "TrvlYr_avg" <= 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}GDO_{navn_fid}_fastekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[2].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_faste','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "AVEGVD_MM_"*-365.25 >= {meanvalues[2]} AND "AVEAGE[Y]" <= 30 THEN 3\r\nWHEN "AVEGVD_MM_"*-365.25 >= {meanvalues[2]} AND "AVEAGE[Y]" <= 100 THEN 2\r\nWHEN "AVEGVD_MM_"*-365.25 >= {meanvalues[2]} AND "AVEAGE[Y]" <= 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}STOK_{navn_fid}_fastekriterier.gpkg'})
                processing.run("native:fieldcalculator", {'INPUT':QgsProcessingFeatureSourceDefinition(layers[3].source(), selectedFeaturesOnly=True, featureLimit=-1, geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid),'FIELD_NAME':'Klasse_faste','FIELD_TYPE':0,'FIELD_LENGTH':0,'FIELD_PRECISION':0,'FORMULA':f'CASE \r\nWHEN "GVD2REG_MM"*-365.25 >= {meanvalues[3]} AND "AVERAGEAGE" <= 30 THEN 3\r\nWHEN "GVD2REG_MM"*-365.25 >= {meanvalues[3]} AND "AVERAGEAGE" <= 100 THEN 2\r\nWHEN "GVD2REG_MM"*-365.25 >= {meanvalues[3]} AND "AVERAGEAGE" <= 200 THEN 1 ELSE 0\r\nEND','OUTPUT':f'{og_path}LIKELYWELLFIELD_{navn_fid}_fastekriterier.gpkg'})



            except:
                    pass
        
