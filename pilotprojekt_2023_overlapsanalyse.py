import os
import time

import geopandas as gpd


def union_dataframe(gdf):
    # check if geometries are valid
    gdf = gdf[gdf.geometry.is_valid]

    # the union of all geometries
    union_geometry = gdf.union_all()
    union_gdf = gpd.GeoDataFrame(geometry=[union_geometry], crs=gdf.crs)

    return union_gdf


def intersect_iol(gdf, gdf_iol):
    gdf_overlap_iol = gpd.overlay(gdf, gdf_iol, how='intersection', keep_geom_type=True, make_valid=True)

    return gdf_overlap_iol


def calculate_intersection_area(gdf, gdf_overlap):
    # Dissolve the geometries of the two geo-dataframe
    gdf = union_dataframe(gdf)
    gdf_overlap = union_dataframe(gdf_overlap)

    # Calculate the area of the original geometry in hectares
    area_hectares = gdf.area.sum() / 10000

    # Get the intersection of the two GeoDataFrames and calculate the area in hectares
    intersection = gpd.overlay(gdf, gdf_overlap, how='intersection', keep_geom_type=True, make_valid=True)
    area_hectares_intersection = intersection.area.sum() / 10000

    # Calculate the percentage of the overlap geometry compared to original
    area_hectares_intersection_pct = 100 * area_hectares_intersection / area_hectares

    return area_hectares_intersection, area_hectares_intersection_pct


def overlap_analysis(name_gdf, gdf_for, gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol):
    # forening
    gdf_for = intersect_iol(gdf_for, gdf_iol)
    for_nfi, for_nfi_pct = calculate_intersection_area(gdf_for, gdf_nfi)
    for_skov, for_skov_pct = calculate_intersection_area(gdf_for, gdf_skov)
    for_bebyg, for_bebyg_pct = calculate_intersection_area(gdf_for, gdf_bebyg)
    for_mark, for_mark_pct = calculate_intersection_area(gdf_for, gdf_marker2023)

    print(f'\n{name_gdf}')
    print('Foreningsmængde')
    print(f'NFI: {for_nfi.round(-2)} ({for_nfi_pct.round(0)}%)')
    print(f'SKOV: {for_skov.round(-2)} ({for_skov_pct.round(0)}%)')
    print(f'BEBYGGELSE: {for_bebyg.round(-2)} ({for_bebyg_pct.round(0)}%)')
    print(f'MARKER: {for_mark.round(-2)} ({for_mark_pct.round(0)}%)')


def run_overlap_analysis(path_nfi, path_skov, path_bebyg, path_marker2023, path_iol,
                         path_sgo):

    gdf_nfi = gpd.read_file(path_nfi)
    gdf_skov = gpd.read_file(path_skov)
    gdf_bebyg = gpd.read_file(path_bebyg)
    gdf_marker2023 = gpd.read_file(path_marker2023)
    gdf_iol = gpd.read_file(path_iol)

    # gvd_filter
    gdf_sgo = gpd.read_file(path_sgo)

    overlap_analysis('Sårbare Grundvandsdannende Områder', gdf_sgo,
                     gdf_nfi, gdf_skov, gdf_bebyg, gdf_marker2023, gdf_iol)



start_time = time.time()  # start timer

root_path = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata'
sgo_path = './output_data'
# paths for common quantity
path_sgo = os.path.abspath(f'{sgo_path}/SårbareGrundvandsdannendeOmråder.gpkg')

# path for externally gis data for overlap analysis
path_nfi = fr'{root_path}\Områder\Andre\NFI_kungeometri.gpkg'
path_skov = fr'{root_path}\Områder\Andre\Skov_2023_kungeometri.gpkg'
path_bebyg = fr'{root_path}\Områder\Andre\Bebyggelse_kungeometri.gpkg'
path_marker2023 = fr'{root_path}\Områder\Andre\Marker_kungeometri.gpkg'
path_iol = fr'{root_path}\Områder\IOL.gpkg'

run_overlap_analysis(
    path_nfi, path_skov, path_bebyg, path_marker2023, path_iol,
    path_sgo
)

end_time = time.time()  # end timer
execution_time = end_time - start_time
print(f"\nExecution time: {execution_time:.2f} seconds")
