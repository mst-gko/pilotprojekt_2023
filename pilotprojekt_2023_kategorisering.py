import os
import shutil
import time
import concurrent.futures
from decimal import Decimal

import pandas as pd
import geopandas as gpd
import numpy as np
# import openpyxl  # needed in py-env as engine for pandas' to_excel function


class AquiferClassification:

    def __init__(self, aquifer_name, path_reffilter, path_refmag, path_stokmag, path_stokfilter, path_iol, path_output='./'):
        """
        Initiates the class
        :param aquifer_name: [string] name of the specific aquifer
        :param path_gdo: [string] path to the geofile containing GDO
        :param path_gvd: [string] path to the geofile containing GVD
        :param path_stok: [string] path to the geofile containing stok
        :param path_likely: [string] path to the geofile containing likely
        :param path_output: [string] path of the exported files
        """
        self.aquifer_name = aquifer_name
        self.path_output = path_output

        self.gdf_iol = gpd.read_file(path_iol)

        self.gdf_refmag= self.get_geodataframe(path_refmag, 'refmag')
        self.gdf_reffilter = self.get_geodataframe(path_reffilter, 'reffilter')
        self.gdf_stokmag = self.get_geodataframe(path_stokmag, 'stokmag')
        self.gdf_stokfilter = self.get_geodataframe(path_stokfilter, 'stokfilter')


        self.gdf_merged = self.merge_dataframes()


        self.perform_data_wrangle()
        self.iterate_iols()
        self.calculate_entire_model()


    @staticmethod
    def get_geodataframe(path_file,suffix):
        """
        This function input a geo file (e.g. shp, geopackage) and outputs a geopandas dataframe
        In the meantime it renames the columns and standardizes data
        :param path_file: path for the geo file
        :param suffix: name of the dataset e.g. '_stok'
        :return: geopandas dataframe
        """
        # import for geopandas dataframe
        gdf = gpd.read_file(path_file)



        # Just add specific names from the dataset to ensure correct naming
        columns_to_rename = {
            'Cell_ID': 'Cell_ID',
            'CELLID': 'Cell_ID',

            'GVD': 'zFlux',

            'GVD_3D': '3DGVD',
            '3DGVD': '3DGVD',
            '3DGVD_mean': '3DGVD',
            '3D_GVD': '3DGVD',
            'AVEGVD_MM_': '3DGVD',
            #  'GVD2REG_MM': '3DGVD',
            'TrvlYr_mean': 'Trvl',
            'TrvlYr_avg': 'Trvl',
            'Trvl_mean': 'Trvl',
            'AVEAGE[Y]': 'Trvl',
            'AVERAGEAGE': 'Trvl',
            'PctPT_mean': 'PctPT',
            'PctPT': 'PctPT',
            'LIKELY': 'PctPT',
            'LIKELYHOOD': 'PctPT'
        }

        for col in gdf.columns:
            # Check if the column name matches any of the keys in the rename dictionary
            for pattern, new_name in columns_to_rename.items():
                if pattern == col:
                    if pattern in ['AVEGVD_MM_', 'GVD2REG_MM']:
                        # Multiply the values by 365 before renaming
                        gdf[col] = gdf[col] * -365

                    # Rename the column to '3D_GVD'
                    gdf = gdf.rename(columns={col: f'{new_name}_{suffix}'})
        return gdf

    @staticmethod
    def drop_geom(gdf):
        """
        This method inputs a geopandas dataframe and removes the geometry if it exists
        :param gdf: geopandas dataframe
        :return: pandas dataframe
        """
        # drop the geometry column if it exists
        if 'geometry' in gdf.columns:
            gdf = gdf.drop(columns=['geometry'])

        return gdf

    def merge_dataframes(self):
        """
        Merge grundvandsdannelse, grundvandsdannende opland, stokatiske kørsler and the likely wellfield
        The geometry of the gvd gdf is used rest of geometries are dropped
        :return: merged geopandas dataframe
        """
        #with pd.option_context('display.max_rows', None, 'display.max_columns',
         #                      None):  # more options can be specified also
          #  print(self.gdf_refmag.head(5))



        df_z1 = pd.merge(
            self.gdf_refmag, self.drop_geom(self.gdf_reffilter),
            left_on='Cell_ID_refmag', right_on='Cell_ID_reffilter',
            how='outer', suffixes=('_refmag', '_reffilter')
        )

        df_z2 = pd.merge(
            df_z1, self.drop_geom(self.gdf_stokmag),
            left_on='Cell_ID_refmag', right_on='Cell_ID_stokmag',
            how='outer', suffixes=('', '_stokmag')
        )

        gdf_merged = pd.merge(
            df_z2, self.drop_geom(self.gdf_stokfilter),
            left_on='Cell_ID_refmag', right_on='Cell_ID_stokfilter',
            how='outer', suffixes=('', '_stokfilter')
        )

        return gdf_merged

    def export_merged_dataframe_as_geopackage(self):
        output_path = f'{self.path_output}/HeleModelområde/{self.aquifer_name}_classification_merged.gpkg'
        self.gdf_merged.to_file(output_path, driver='GPKG')

    def export_dataframe_as_excel(self, dataframe, folderstructure, name):
        """
        Exports the merged geopandas dataframe as an MS Excel spreadsheet
        """
        file_path = f'{self.path_output}/{folderstructure}/{name}.xlsx'
        dataframe.to_excel(file_path, engine="openpyxl")

    def export_dataframe_as_geopackage(self, dataframe, folderstructure, name):
        """
        Exports the merged geopandas dataframe as a geopackage
        """
        output_path = f'{self.path_output}/{folderstructure}{name}.gpkg'
        dataframe.to_file(output_path, driver='GPKG')

    def calculate_static_q10(self, dataframe, colname_output, colname_3dgvd):
        q_gvd3d = 10  # quantiles for 3d gvd e.g. 50 => 50% quantile

        name_g = f'{colname_output}_gvd3d_static_q{q_gvd3d}'
        dataframe[name_g] = self.calculate_nonzero_quantile(
            df=dataframe, column_name=colname_3dgvd, q=q_gvd3d)

    @staticmethod
    def calculate_nonzero_quantile(df, column_name, q=10):
        """
        Calculate a given quantile from positive values of a column within a dataframe
        :param df: pandas dataframe
        :param column_name: name of column in the pandas dataframe
        :param q: the desired quantile e.g. 10 as the 10% quantile of the data
        :return: the number of the quantile of the data
        """


        non_zero_values = df[df[column_name] != 0].dropna(subset=[column_name])

        if non_zero_values.empty:
            return np.nan  # Return NaN if no valid values exist

        quantile = np.percentile(non_zero_values[column_name], q=q)

        return quantile

    def perform_data_wrangle(self):
        """
        Performs data wrangle on the merged dataframe
        1) calculate gvd 3d for a year for the stochastic dataset
        2) calculate gvd 3d for a year for the likelyhood dataset
        """
        # calculate gvd 3d for a year for the stochastic dataset


        self.calculate_static_q10(dataframe=self.gdf_merged, colname_output='refmag', colname_3dgvd='3DGVD_refmag')

        # GDO
        self.calculate_static_q10(dataframe=self.gdf_merged, colname_output='reffilter', colname_3dgvd='3DGVD_reffilter')

        # STOCHASTIC
        self.calculate_static_q10(dataframe=self.gdf_merged, colname_output='stokmag', colname_3dgvd='3DGVD_stokmag')

        # LIKELYWELLFIELD
        self.calculate_static_q10(dataframe=self.gdf_merged, colname_output='stokfilter', colname_3dgvd='3DGVD_stokfilter')

    def iterate_iols(self, target_crs='EPSG:25832'):
       # self.gdf_merged_copy = gpd.GeoDataFrame(data=None, columns=self.gdf_merged.columns, index=self.gdf_merged.index)

        if self.gdf_iol.crs != target_crs:
            self.gdf_iol = self.gdf_iol.to_crs(target_crs)
        else:
            self.gdf_iol = self.gdf_iol

        if self.gdf_merged.crs != target_crs:
            self.gdf_merged = self.gdf_merged.to_crs(target_crs)
        else:
            self.gdf_merged = self.gdf_merged


        for i, (idx, iol) in enumerate(self.gdf_iol.iterrows()):
            gdf_iol_single = gpd.GeoDataFrame([iol], geometry='geometry', crs=self.gdf_iol.crs)
            gdf_plant = self.gdf_merged.sjoin(gdf_iol_single, how="inner")

            self.add_classification_static(gdf_plant)
            self.add_classification_relative(gdf_plant)
            self.export_dataframe_as_geopackage(dataframe=gdf_plant, folderstructure=f'Anlæg/', name=f'{self.aquifer_name}_{iol["anl_id"]}')
            if i == 0:
                self.gdf_merged_copy = gdf_plant
            else:
                self.gdf_merged_copy = pd.concat([self.gdf_merged_copy, gdf_plant])

        self.add_sgo(self.gdf_merged_copy)
        self.export_dataframe_as_geopackage(dataframe=self.gdf_merged_copy, folderstructure=f'IOL/',name=f'{self.aquifer_name}')

    def calculate_entire_model(self):
        self.add_classification_static(self.gdf_merged)
        self.add_classification_relative(self.gdf_merged)
        self.add_sgo(self.gdf_merged)

    def classification_relative(self, dataframe, colname_output, colname_3dgvd, colname_trvl, colname_likely):
        """
        Performs the relative classification. NB the order for the classification is important. E.g. if class 1 is
        generated after class 3 then all within class 3 will be changed to class 1 since the requirements are lower.
        :param colname_output: the name of the dataset used for creating the output column name
        :param colname_3dgvd: the column name of the input-data 3d-gvd
        :param colname_trvl: the column name of the input-data traveltime
        :param colname_likely: the column name of the input-data likelyhood
        """
        likely_limit = 0.25  # the limit of the likelyhood
        trvl_limit = 200  # the limit of the traveltime
        q_trvl = (50, 30, 20)  # quantiles for traveltime e.g. 50 => 50% quantile
        q_gvd3d = (10, 25, 50)  # quantiles for 3d gvd e.g. 50 => 50% quantil
        cat_r = 0  # initial category
        name_cat = f'{colname_output}_classify_relative'


        dataframe[name_cat] = cat_r
        for q_t, q_g in zip(q_trvl, q_gvd3d):
            cat_r += 1
            name_t = f'{colname_output}_trvl_relative_q{q_t}'
            name_g = f'{colname_output}_gvd3d_relative_q{q_g}'
            dataframe[name_t] = self.calculate_nonzero_quantile(
                df=dataframe, column_name=colname_trvl, q=q_t)
            dataframe[name_g] = self.calculate_nonzero_quantile(
                df=dataframe, column_name=colname_3dgvd, q=q_g)
            dataframe.loc[
                (dataframe[colname_likely] > likely_limit) &
                (dataframe[colname_trvl] > 0) &
                (dataframe[colname_trvl] < trvl_limit) &
                (dataframe[colname_3dgvd] >= dataframe[name_g]) &
                (dataframe[colname_trvl] <= dataframe[name_t]),
                name_cat] = cat_r

    def classification_static(self, dataframe, colname_output, colname_3dgvd, colname_trvl):
        """
        Performs the static classification. NB the order for the classification is important. E.g. if class 1 is
        generated after class 3 then all within class 3 will be changed to class 1 since the requirements are lower.
        :param colname_output: the name of the dataset used for creating the output column name
        :param colname_3dgvd: the column name of the input-data 3d-gvd
        :param colname_trvl: the column name of the input-data traveltime
        """
        trvl_limit_lst = (200, 100, 30)  # limit for traveltime
        cat_s = 0  # initial category
        name_g = f'{colname_output}_gvd3d_static_q10'
        name_cat = f'{colname_output}_classify_static'

        # classify static
        dataframe[name_cat] = cat_s
        for trvl_limit in trvl_limit_lst:
            cat_s += 1
            dataframe.loc[
                (dataframe[colname_trvl] > 0) &
                (dataframe[colname_trvl] <= trvl_limit) &
                (dataframe[colname_3dgvd] >= dataframe[name_g]),
                name_cat] = cat_s

    def classification_sgo(self, dataframe, colname_likely_static, colname_likely_relative):
        """
        Performs the SGO classification.
        :param colname_likely_static: the column name of the static classification of the likely wellfield dataset
        :param colname_likely_relative: the column name of the relative classification of the likely wellfield dataset
        """

        name_sgo = f'SGO'
        dataframe[name_sgo] = False

        dataframe.loc[
                (dataframe[colname_likely_static] >= 2) |
                (dataframe[colname_likely_relative] >= 2),
                name_sgo] = True

    def add_classification_static(self, dataframe):
        """
        Runs the static classification for all dataset associated with the given aquifer
        """
        # GVD
        self.classification_static(dataframe=dataframe, colname_output='refmag', colname_3dgvd='3DGVD_refmag', colname_trvl='Trvl_refmag')

        # GDO
        self.classification_static(dataframe=dataframe, colname_output='reffilter', colname_3dgvd='3DGVD_reffilter', colname_trvl='Trvl_reffilter')

        # STOCHASTIC
        self.classification_static(dataframe=dataframe, colname_output='stokmag', colname_3dgvd='3DGVD_stokmag', colname_trvl='Trvl_stokmag')

        # LIKELYWELLFIELD
        self.classification_static(dataframe=dataframe, colname_output='stokfilter', colname_3dgvd='3DGVD_stokfilter', colname_trvl='Trvl_stokfilter')

    def add_classification_relative(self, dataframe):
        """
        Runs the relative classification for all dataset associated with the given aquifer
        """
        # GVD
        self.classification_relative(dataframe=dataframe,
            colname_output='refmag', colname_3dgvd='3DGVD_refmag',
            colname_trvl='Trvl_refmag', colname_likely='PctPT_refmag'
        )

        # GDO
        self.classification_relative(dataframe=dataframe,
            colname_output='reffilter', colname_3dgvd='3DGVD_reffilter',
            colname_trvl='Trvl_reffilter', colname_likely='PctPT_reffilter'
        )

        # STOCHASTIC
        self.classification_relative(dataframe=dataframe,
            colname_output='stokmag', colname_3dgvd='3DGVD_stokmag',
            colname_trvl='Trvl_stokmag', colname_likely='PctPT_stokmag'
        )

        # LIKELYWELLFIELD
        self.classification_relative(dataframe=dataframe,
            colname_output='stokfilter', colname_3dgvd='3DGVD_stokfilter',
            colname_trvl='Trvl_stokfilter', colname_likely='PctPT_stokfilter')


    def add_sgo(self, dataframe):
        """
        Runs the combined SGO classification to identify if it is SGO (sårbare grundvandsdannende områder)
        """
        self.classification_sgo(dataframe=dataframe,
            colname_likely_static='stokfilter_classify_static', colname_likely_relative='stokfilter_classify_relative'
        )


class SGO_Classification:
    def __init__(self, input_paths, path_output='./'):
        self.input_paths = input_paths
        self.path_output = path_output
        self.dataframes = [self.get_geodataframe(path) for path in self.input_paths]
        self.gpd_sgo = gpd.GeoDataFrame(data=None, columns=self.dataframes[0].columns, index=self.dataframes[0].index)

    @staticmethod
    def get_geodataframe(path_file):
        """
        This function input a geo file (e.g. shp, geopackage) and outputs a geopandas dataframe
        :param path_file: path for the geo file
        :return: geopandas dataframe
        """
        #Get the aquifer name
        aquifer_name = os.path.splitext(os.path.basename(path_file))[0]

        # import for geopandas dataframe
        gdf = gpd.read_file(path_file)

        gdf['magasin'] = aquifer_name

        return gdf

    def combine_and_flatten_sgo(self):

        self.gpd_sgo = pd.concat(self.dataframes, ignore_index=True)

        self.gpd_sgo = self.gpd_sgo.rename(columns={'Cell_ID_stokfilter': 'Cell_ID',
                                                    #'zFlux_refmag': 'zFlux_ref', 'zFlux_stokmag': 'zFlux_stok',
                                                    'refmag_classify_static': 'Refmag_faste',
                                                    'refmag_classify_relative': 'Refmag_relative',

                                                    'reffilter_classify_static': 'Reffilter_faste',
                                                    'reffilter_classify_relative': 'Reffilter_relative',

                                                    'stokmag_classify_static': 'Stokmag_faste',
                                                    'stokmag_classify_relative': 'Stokmag_relative',

                                                    'stokfilter_classify_static': 'Stokfilter_faste',
                                                    'stokfilter_classify_relative': 'Stokfilter_relative',
                                                    })

        # Step 2: Compute the combined classification
        self.gpd_sgo['combined_classification'] = self.gpd_sgo["Stokfilter_faste"] + self.gpd_sgo["Stokfilter_relative"]

        # Step 3: Group by 'Cell_ID' and keep the row with the highest combined classification (FOR SIMPLE SGO)
        gdf_filtered = self.gpd_sgo.loc[self.gpd_sgo.groupby('Cell_ID')['combined_classification'].idxmax()].reset_index(drop=True)

        # Step 4: Further filter to keep only the rows where 'SGO' is True (FOR SIMPLE SGO)
        gdf_filtered_sgo_true = gdf_filtered[gdf_filtered['SGO'] == True].drop_duplicates(subset=['Cell_ID'])

        # Step 5: Remove duplicates by keeping only 'Cell_ID' and 'geometry' (FOR SIMPLE SGO)
        self.gpd_sgo_simplified = gdf_filtered_sgo_true[['geometry']]

        # Step 6: Only save needed columns for complete SGO-file (for database)
        self.gpd_sgo = self.gpd_sgo[['Cell_ID', 'magasin', 'anl_id', 'anl_navn',
                                              '3DGVD_refmag', '3DGVD_reffilter', '3DGVD_stokmag','3DGVD_stokfilter',
                                              'PctPT_refmag', 'PctPT_reffilter', 'PctPT_stokmag', 'PctPT_stokfilter',
                                              'Trvl_refmag', 'Trvl_reffilter', 'Trvl_stokmag', 'Trvl_stokfilter',
                                              'Refmag_relative', 'Reffilter_relative', 'Stokmag_relative', 'Stokfilter_relative',
                                              'Refmag_faste', 'Reffilter_faste', 'Stokmag_faste', 'Stokfilter_faste', 'SGO',
                                              'geometry']]


        # Step 7: Save the 2 result to GeoPackages
        output_path = f'{self.path_output[0]}/SårbareGrundvandsdannendeOmråder.gpkg'
        self.gpd_sgo.to_file(output_path, driver='GPKG')

        output_path2 = f'{self.path_output[0]}/SårbareGrundvandsdannendeOmråder_simplificeret.gpkg'
        self.gpd_sgo_simplified.to_file(output_path2, driver='GPKG')


def run_script(input_data_lst):
    """
    Function solely used when running the script concurrently
    Export a geopackage but not an Excel file per default
    :param input_data_lst: list containing all input data for an aquifer
    """
    AquiferClassification(
        aquifer_name=input_data_lst[0], path_reffilter=input_data_lst[1], path_refmag=input_data_lst[2],
        path_stokmag=input_data_lst[3], path_stokfilter=input_data_lst[4], path_iol=input_data_lst[5],
        path_output=input_data_lst[6]
    ).export_merged_dataframe_as_geopackage()


def run_script_concurrently(inputs_lst, sgo_input_paths, sgo_output_path):
    """
    Function for running the script concurrently
    :param inputs_lst: list of lists containing all input data for each aquifer to do AquiferClassification
    :param sgo_input_paths: list of dataframes to combine during the SGO classification
    :param sgo_output_path: list of output paths
    """
    # First part: Run the script for each input in inputs_lst concurrently
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit the tasks for each input in inputs_lst
        futures_first = [executor.submit(run_script, data_lst) for data_lst in inputs_lst]

        #concurrent.futures.wait(futures_first)
        # Wait for all tasks in the first part to complete and get their results to ensure completion
        for future in futures_first:
            future.result()  # This will block until each task completes successfully

    # Second part: Only starts after the first part has fully completed
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit the task for SGO classification
        future_second = executor.submit(
            SGO_Classification(input_paths=sgo_input_paths, path_output=sgo_output_path).combine_and_flatten_sgo()
        )

        # Wait for the second part to complete
        concurrent.futures.wait([future_second])



start_time = time.time()  # start timer

root_path_output = './output_data/'

# Aquifer 1
aquifer_name_sand1 = 'Sand1'
path_gdo_sand1 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GDO\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand1.shp'
path_gvd_sand1 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GVD\Fyn_3D_GVD_RegCode1_PT_lag1B_mult_zlag1.shp'
path_stok_sand1 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\STOK\Stats_RegCode_1_sel.shp'
path_likely_sand1 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\LIKELYWELL\Likely_Wellfield_beregninger.shp'
path_iol_sand1 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\Områder\Opdelte_IOL\Sand1_IOL_BUFFER.gpkg'
data_aquifer1 = [
    aquifer_name_sand1, path_gdo_sand1, path_gvd_sand1, path_stok_sand1,
    path_likely_sand1, path_iol_sand1,
    root_path_output
]

# Aquifer 2
aquifer_name_sand2 = 'Sand2'
path_gdo_sand2 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GDO\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand2.shp'
path_gvd_sand2 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GVD\Fyn_3D_GVD_RegCode2_PT_lag1B_mult_zlag1.shp'
path_stok_sand2 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\STOK\Stat_RegCode_2_sel.shp'
path_likely_sand2 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\LIKELYWELL\Likely_Wellfield_beregninger.shp'
path_iol_sand2 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\Områder\Opdelte_IOL\Sand2_IOL_BUFFER.gpkg'
data_aquifer2 = [
    aquifer_name_sand2, path_gdo_sand2, path_gvd_sand2, path_stok_sand2,
    path_likely_sand2, path_iol_sand2,
    root_path_output
]

# Aquifer 3
aquifer_name_sand3 = 'Sand3'
path_gdo_sand3 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GDO\Fyn_GDO_PT_Lag1B_Wells_Stat_Sand3.shp'
path_gvd_sand3 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GVD\Fyn_3D_GVD_RegCode3_PT_lag1B_mult_zlag1.shp'
path_stok_sand3 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\STOK\Stat_RegCode_3_sel.shp'
path_likely_sand3 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\LIKELYWELL\Likely_Wellfield_beregninger.shp'
path_iol_sand3 = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\Områder\Opdelte_IOL\Sand3_IOL_BUFFER.gpkg'
data_aquifer3 = [
    aquifer_name_sand3, path_gdo_sand3, path_gvd_sand3, path_stok_sand3,
    path_likely_sand3, path_iol_sand3,
    root_path_output
]

# Aquifer 4
aquifer_name_kalk = 'Kalk'
path_gdo_kalk = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GDO\Fyn_GDO_PT_Lag1B_Wells_Stat_DK.shp'
path_gvd_kalk = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\GVD\Fyn_3D_GVD_RegCode4_PT_lag1B_mult_zlag1.shp'
path_stok_kalk = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\STOK\Stat_RegCode_4_sel.shp'
path_likely_kalk = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\LIKELYWELL\Likely_Wellfield_beregninger.shp'
path_iol_kalk = r'F:\GKO\admin\tvaerg\Pilotprojekter 2023\SPOR 1_Identifikation af områder med behov for målrettet grundvandsbeskyttelse\27_Data Final\Rådata\Områder\Opdelte_IOL\Kalk_IOL_BUFFER.gpkg'
data_aquifer4 = [
    aquifer_name_kalk, path_gdo_kalk, path_gvd_kalk, path_stok_kalk,
    path_likely_kalk, path_iol_kalk,
    root_path_output
]


# pack data for all aquifers in a list of lists and run script for all aquifers concurrently
data_aquifer_all = [data_aquifer1, data_aquifer2, data_aquifer3, data_aquifer4]


sgo_input_paths = []
sgo_output_path = []
for aq in range(len(data_aquifer_all)):
    sgo_input_paths.append(os.path.abspath(f'{root_path_output}IOL/{data_aquifer_all[aq][0]}.gpkg'))
    sgo_output_path.append(root_path_output)



# List of directories to clear
directories = [
    './output_data/Anlæg/',
    './output_data/HeleModelområde/',
    './output_data/IOL/'
]
def ensure_directories_exist(dirs):
    for dir_path in dirs:
        # Create the directory if it doesn't exist
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            print(f"Directory {dir_path} created.")
        else:
            print(f"Directory {dir_path} already exists.")

def clear_directories(dirs):
    for dir_path in dirs:
        if os.path.exists(dir_path):
            # Remove all files in the directory
            for file_name in os.listdir(dir_path):
                file_path = os.path.join(dir_path, file_name)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)  # Remove the file
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)  # Remove a subdirectory and its contents
                except Exception as e:
                    print(f'Failed to delete {file_path}. Reason: {e}')
        else:
            print(f'Directory {dir_path} does not exist.')

# Ensure directories exist before clearing them
ensure_directories_exist(directories)

# Clear the specified directories when the script starts
clear_directories(directories)


run_script_concurrently(inputs_lst=data_aquifer_all, sgo_input_paths=sgo_input_paths, sgo_output_path=sgo_output_path)

end_time = time.time()  # end timer
execution_time = end_time - start_time
print(f"Execution time: {execution_time:.2f} seconds")
